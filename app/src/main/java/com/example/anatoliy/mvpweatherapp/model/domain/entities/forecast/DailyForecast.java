package com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast;

import com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.BaseViewHolder;
import com.example.anatoliy.mvpweatherapp.presentation.ui.enums.ViewHolderItemIds;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DailyForecast implements BaseViewHolder.Item{

    private double temperatureMax;
    private double temperatureMin;
    private long date;
    private String description;

    private List<ExtendedWeather> hourlyForecast;

    public double getTemperatureMax() {
        return temperatureMax;
    }

    public void setTemperatureMax(double temperatureMax) {
        this.temperatureMax = temperatureMax;
    }

    public double getTemperatureMin() {
        return temperatureMin;
    }

    public void setTemperatureMin(double temperatureMin) {
        this.temperatureMin = temperatureMin;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ExtendedWeather> getHourlyForecast() {
        return hourlyForecast;
    }

    public void add(ExtendedWeather weather){
        if (hourlyForecast == null)
            hourlyForecast = new ArrayList<>();
        hourlyForecast.add(weather);
    }

    @Override
    public int getViewType() {
        return ViewHolderItemIds.DAILY_FORECAST;
    }
}
