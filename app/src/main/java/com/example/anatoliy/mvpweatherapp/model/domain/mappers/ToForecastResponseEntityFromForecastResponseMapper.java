package com.example.anatoliy.mvpweatherapp.model.domain.mappers;

import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.ForecastResponse;
import com.example.anatoliy.mvpweatherapp.model.domain.Mapper;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.DailyForecast;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.ForecastResponseEntity;

import java.util.List;

public class ToForecastResponseEntityFromForecastResponseMapper implements Mapper<ForecastResponse, ForecastResponseEntity> {
    private final Mapper<ForecastResponse, List<DailyForecast>> toDailyForecastMapper;
    private final Mapper<ForecastResponse, SettlementWeatherEntity> toSettlementWeatherEntityMapper;

    public ToForecastResponseEntityFromForecastResponseMapper(Mapper<ForecastResponse, List<DailyForecast>> toDailyForecastMapper,
                                                              Mapper<ForecastResponse, SettlementWeatherEntity> toSettlementWeatherEntityMapper) {
        this.toDailyForecastMapper = toDailyForecastMapper;
        this.toSettlementWeatherEntityMapper = toSettlementWeatherEntityMapper;
    }

    @Override
    public ForecastResponseEntity map(ForecastResponse response) {
        return new ForecastResponseEntity(
                toDailyForecastMapper.map(response),
                toSettlementWeatherEntityMapper.map(response)
        );
    }
}
