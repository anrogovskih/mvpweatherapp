package com.example.anatoliy.mvpweatherapp.utils;

import android.content.Context;

import com.example.anatoliy.mvpweatherapp.R;

public class FormatUtils {

    /**
     * Formats temperature in Kelvins into Celsius with all necessary concatenations
     * @param c - context to access app resources
     * @param temperature - in Kelvins
     * @return string to set in TextView
     */
    public static String getTemperatureCelsius(Context c, double temperature){
        int celsiusT = (int) (temperature - 273.15);
        return celsiusT > 0 ?
                c.getString(R.string.temperature_plus_celsius_format, celsiusT) :
                c.getString(R.string.temperature_other_celsius_format, celsiusT);
    }
}
