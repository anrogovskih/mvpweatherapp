package com.example.anatoliy.mvpweatherapp.model.domain.mappers;

import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.Forecast;
import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.GroupsResponse;
import com.example.anatoliy.mvpweatherapp.model.domain.Mapper;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;

import java.util.ArrayList;
import java.util.List;

public class ToSettlementWeatherEntitiesFromGroupsResponseMapper
        implements Mapper<GroupsResponse, List<SettlementWeatherEntity>> {
    @Override
    public List<SettlementWeatherEntity> map(GroupsResponse response) {
        int size = response.getForecast() != null? response.getForecast().size() : 0;
        List<SettlementWeatherEntity> result = new ArrayList<>(size);
        if (size > 0){
            for (Forecast weather:response.getForecast()) {
                SettlementWeatherEntity entity = new SettlementWeatherEntity();
                entity.currentTemperature = weather.getMain().getTemp();
                entity.name = weather.getName();
                entity.settlementId = weather.getId();
                entity.updateTimeStamp = System.currentTimeMillis();
                result.add(entity);
            }
        }
        return result;
    }
}
