package com.example.anatoliy.mvpweatherapp.model.data.prefs;

public interface SharedRepository {
    /**
     * Set timestamp of the last successful network update.
     */
    void setLastUpdateTimestamp(long lastUpdateTimestamp);

    /**
     * Get timestamp of the last successful network update.
     */
    long getLastUpdateTimestamp();
}
