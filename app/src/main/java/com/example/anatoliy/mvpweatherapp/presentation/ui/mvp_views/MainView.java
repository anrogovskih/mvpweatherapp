package com.example.anatoliy.mvpweatherapp.presentation.ui.mvp_views;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;
import com.example.anatoliy.mvpweatherapp.presentation.ui.enums.ContentState;

import java.util.List;

public interface MainView extends MvpView {

    /**
     * Sets this view into one of 4 base states
     */
    @StateStrategyType(AddToEndSingleStrategy.class)
    void setState(@ContentState String state);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setData(List<SettlementWeatherEntity> settlements);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setLoading(boolean isLoading);
}
