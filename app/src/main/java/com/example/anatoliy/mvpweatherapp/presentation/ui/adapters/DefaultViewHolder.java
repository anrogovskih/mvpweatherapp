package com.example.anatoliy.mvpweatherapp.presentation.ui.adapters;

import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

/**
 * Just a dummy view holder, that should never be created. Saves app from crushing if an unexpected
 * view type received in onCreateViewHolder method.
 */
public class DefaultViewHolder extends BaseViewHolder {
    private DefaultViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    public static DefaultViewHolder newInstance(ViewGroup parent){
        return new DefaultViewHolder(createView(parent, android.R.layout.simple_list_item_1));
    }
}
