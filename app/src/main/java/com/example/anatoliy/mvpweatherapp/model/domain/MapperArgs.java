package com.example.anatoliy.mvpweatherapp.model.domain;

/**
 * Transforms one object to another
 * @param <From> - input object
 * @param <To> - output object
 * @param <Argument> - additional data for mapping
 */
public interface MapperArgs<From, To, Argument> {
    To map(From object, Argument argument);
}
