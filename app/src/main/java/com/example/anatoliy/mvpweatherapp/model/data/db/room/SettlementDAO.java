package com.example.anatoliy.mvpweatherapp.model.data.db.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface SettlementDAO {
    @Query("SELECT * FROM settlementweatherentity")
    List<SettlementWeatherEntity> getAll();

    @Query("SELECT * FROM settlementweatherentity WHERE settlementId IN (:settlementIds)")
    List<SettlementWeatherEntity> loadAllByIds(int[] settlementIds);

    @Query("SELECT * FROM settlementweatherentity WHERE name LIKE :name LIMIT 1")
    SettlementWeatherEntity findByName(String name);

    @Query("SELECT * FROM settlementweatherentity WHERE settlementId LIKE :id LIMIT 1")
    SettlementWeatherEntity findById(long id);

    @Insert(onConflict = REPLACE)
    void insertAll(SettlementWeatherEntity... settlements);

    @Delete
    void delete(SettlementWeatherEntity settlement);

    @Query("DELETE FROM SettlementWeatherEntity")
    void dropTable();
}
