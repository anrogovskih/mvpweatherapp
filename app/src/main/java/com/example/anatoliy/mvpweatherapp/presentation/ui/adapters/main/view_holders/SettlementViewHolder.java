package com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.main.view_holders;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.anatoliy.mvpweatherapp.R;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;
import com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.BaseViewHolder;
import com.example.anatoliy.mvpweatherapp.utils.FormatUtils;

import butterknife.BindView;

public class SettlementViewHolder extends BaseViewHolder<SettlementWeatherEntity> {

    @BindView(R.id.id__temperature)
    TextView temperature;
    @BindView(R.id.id__name)
    TextView name;

    private SettlementViewHolder(@NonNull View itemView,
                                 View.OnClickListener onClickListener,
                                 View.OnLongClickListener onLongClickListener) {
        super(itemView);
        itemView.setOnClickListener(onClickListener);
        itemView.setOnLongClickListener(onLongClickListener);
    }

    public static SettlementViewHolder newInstance(ViewGroup parent,
                                                   View.OnClickListener onClickListener,
                                                   View.OnLongClickListener onLongClickListener) {
        return new SettlementViewHolder(createView(parent, R.layout.item_settlement), onClickListener, onLongClickListener);
    }

    @Override
    public void bind(SettlementWeatherEntity settlement) {
        super.bind(settlement);
        itemView.setTag(settlement);
        Context c = itemView.getContext();
        name.setText(settlement.name);
        int celsiusT = (int) (settlement.currentTemperature - 273.15);
        temperature.setText(FormatUtils.getTemperatureCelsius(c, settlement.currentTemperature));
        temperature.setBackgroundResource(celsiusT > 0 ? R.drawable.hot_temp_bg : R.drawable.cold_temp_bg);
    }
}
