package com.example.anatoliy.mvpweatherapp.model.data.web;

import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.ForecastResponse;
import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.GroupsResponse;
import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.WeatherResponse;

import io.reactivex.Single;

public interface WebRepository {
    /**
     * Получить погоду для переданного населенного пункта
     * @param query - населенный пункт, введенный пользователем
     */
    Single<WeatherResponse> getWeather(String query);

    /**
     * Получить погоду для переданного населенного пункта
     * @param id - id населенного пункта
     */
    Single<WeatherResponse> getWeather(long id);

    /**
     * Получить прогноз погоды для переданного населенного пункта
     * @param query - населенный пункт, введенный пользователем
     */
    Single<ForecastResponse> getForecast(String query);

    /**
     * Получить прогноз погоды для переданного населенного пункта
     * @param id - id населенного пункта
     */
    Single<ForecastResponse> getForecast(long id);

    /**
     * Получить погоду для списка населенных пунктов
     * @param ids - id населенных пунктов
     */
    Single<GroupsResponse> getGroups(String ids);
}
