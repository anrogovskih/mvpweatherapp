package com.example.anatoliy.mvpweatherapp.model.data.web;

import com.example.anatoliy.mvpweatherapp.model.api_open_weather.OpenWeatherService;
import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.ForecastResponse;
import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.GroupsResponse;
import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.WeatherResponse;

import io.reactivex.Single;

public class WebRepositoryImpl implements WebRepository {
    private final String apiKey;

    public WebRepositoryImpl(String apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    public Single<WeatherResponse> getWeather(String query) {
        return OpenWeatherService.getInstance().getApi().getWeather(apiKey, query);
    }

    @Override
    public Single<ForecastResponse> getForecast(String query) {
        return OpenWeatherService.getInstance().getApi().getForecast(apiKey, query);
    }

    @Override
    public Single<GroupsResponse> getGroups(String ids) {
        return OpenWeatherService.getInstance().getApi().getGroups(apiKey, ids);
    }

    @Override
    public Single<WeatherResponse> getWeather(long id) {
        return OpenWeatherService.getInstance().getApi().getWeather(apiKey, id);
    }

    @Override
    public Single<ForecastResponse> getForecast(long id) {
        return OpenWeatherService.getInstance().getApi().getForecast(apiKey, id);
    }
}
