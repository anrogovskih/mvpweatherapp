package com.example.anatoliy.mvpweatherapp.presentation.presenters;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.anatoliy.mvpweatherapp.model.domain.UseCase;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;
import com.example.anatoliy.mvpweatherapp.presentation.ui.enums.ContentState;
import com.example.anatoliy.mvpweatherapp.presentation.ui.mvp_views.MainView;

import java.util.List;

import io.reactivex.observers.DisposableObserver;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {
    private static final String TAG = MainPresenter.class.getSimpleName();

    //data update interval, 10 minutes *30 sec
    private static final long MIN_UPDATE_INTERVAL = /*30000*/600000;
    //data update interval, one hour *1 min
    private static final long DATA_VALIDITY_INTERVAL = /*60000*/3600000;

    private final UseCase<List<SettlementWeatherEntity>, Void> getSavedSettlementsUC;
    private final UseCase<Void, Void> clearAllUC;
    private final UseCase<Void, SettlementWeatherEntity> deleteSettlementUC;
    private final UseCase<List<SettlementWeatherEntity>, long[]> updateSettlementsUC;

    public MainPresenter(UseCase<List<SettlementWeatherEntity>, Void> getSavedSettlementsUC,
                         UseCase<Void, Void> clearAllUC,
                         UseCase<Void, SettlementWeatherEntity> deleteSettlementUC,
                         UseCase<List<SettlementWeatherEntity>, long[]> updateSettlementsUC) {
        this.getSavedSettlementsUC = getSavedSettlementsUC;
        this.clearAllUC = clearAllUC;
        this.deleteSettlementUC = deleteSettlementUC;
        this.updateSettlementsUC = updateSettlementsUC;
    }

    public void deleteSettlement(SettlementWeatherEntity settlement) {
        deleteSettlementUC.execute(settlement);
    }

    /**
     * Reloads data from database and possibly updates it with network call
     */
    public void updateData() {
        getViewState().setState(ContentState.LOADING);
        getSavedSettlementsUC.execute(new DisposableObserver<List<SettlementWeatherEntity>>() {
            @Override
            public void onNext(List<SettlementWeatherEntity> settlementWeatherEntities) {
                getViewState().setData(settlementWeatherEntities);

                if (settlementWeatherEntities != null && !settlementWeatherEntities.isEmpty()) {
                    getViewState().setState(ContentState.READY);
                    possiblyUpdateWeather(settlementWeatherEntities);
                } else
                    getViewState().setState(ContentState.EMPTY);
            }

            @Override
            public void onError(Throwable e) {
                getViewState().setState(ContentState.ERROR);
                Log.e(TAG, "getSavedSettlementsUC: onError: ", e);
            }

            @Override
            public void onComplete() {

            }
        });
    }

    /**
     * Updates weather if at least one of settlements was updated too long ago.
     * @param settlementWeatherEntities - list of settlements, that we should check
     */
    private void possiblyUpdateWeather(List<SettlementWeatherEntity> settlementWeatherEntities) {
        long lastUpdateTime = Long.MAX_VALUE;
        long[] ids = new long[settlementWeatherEntities.size()];
        for (int i = 0; i < settlementWeatherEntities.size(); i++) {
            SettlementWeatherEntity entity = settlementWeatherEntities.get(i);
            ids[i] = entity.settlementId;
            if (entity.updateTimeStamp < lastUpdateTime) {
                lastUpdateTime = entity.updateTimeStamp;
            }
        }
        long finalLastUpdateTime = lastUpdateTime;
        if (requiresUpdate(lastUpdateTime)){
            getViewState().setLoading(true);
            updateSettlementsUC.execute(new DisposableObserver<List<SettlementWeatherEntity>>() {
                @Override
                public void onNext(List<SettlementWeatherEntity> entities) {
                    getViewState().setLoading(false);
                    getViewState().setData(entities);
                }

                @Override
                public void onError(Throwable e) {
                    getViewState().setLoading(false);
                    if (isDataExpired(finalLastUpdateTime))
                        getViewState().setState(ContentState.ERROR);
                    Log.e(TAG, "updateSettlementsUC: onError: ", e);
                }

                @Override
                public void onComplete() {

                }
            }, ids);
        }
    }

    /**
     * If update was required, but failed, we may not be showing error to user.
     * @return true if it's time to query new data from net
     */
    private boolean requiresUpdate(long lastUpdateTime) {
        return (lastUpdateTime == 0 || System.currentTimeMillis() - MIN_UPDATE_INTERVAL > lastUpdateTime);
    }

    /**
     * If data is expired and failed to update, it's time to show error to user
     * @return true if weather data is outdated and must be reloaded
     */
    private boolean isDataExpired(long lastUpdateTime) {
        return (lastUpdateTime == 0 || System.currentTimeMillis() - DATA_VALIDITY_INTERVAL > lastUpdateTime);
    }

    public void clearSettlements() {
        clearAllUC.execute();
        getViewState().setData(null);
        getViewState().setState(ContentState.EMPTY);
    }

    public void onLastItemDeleted() {
        getViewState().setState(ContentState.EMPTY);
    }
}
