package com.example.anatoliy.mvpweatherapp.model.domain.interactor;

import com.example.anatoliy.mvpweatherapp.model.data.db.DataBaseRepository;
import com.example.anatoliy.mvpweatherapp.model.domain.UseCase;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class GetSavedSettlementsUseCase extends UseCase<List<SettlementWeatherEntity>, Void> {

    private final DataBaseRepository db;

    public GetSavedSettlementsUseCase(Scheduler resultScheduler, Scheduler sourceScheduler, DataBaseRepository db) {
        super(resultScheduler, sourceScheduler);
        this.db = db;
    }

    @Override
    protected Observable<List<SettlementWeatherEntity>> buildUseCaseObservable() {
        return db.getSavedSettlements().toObservable();
    }
}
