package com.example.anatoliy.mvpweatherapp.model.domain.interactor;

import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.ForecastResponse;
import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.WeatherResponse;
import com.example.anatoliy.mvpweatherapp.model.data.web.WebRepository;
import com.example.anatoliy.mvpweatherapp.model.domain.Mapper;
import com.example.anatoliy.mvpweatherapp.model.domain.UseCase;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.WeatherResponseEntity;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;

public class GetSettlementWeatherUseCase extends UseCase<WeatherResponseEntity, SettlementWeatherEntity> {
    private final WebRepository web;
    private final Mapper<WeatherResponse,WeatherResponseEntity> mapper;

    public GetSettlementWeatherUseCase(Scheduler resultScheduler,
                                       Scheduler sourceScheduler,
                                       WebRepository web,
                                       Mapper<WeatherResponse, WeatherResponseEntity> mapper) {
        super(resultScheduler, sourceScheduler);
        this.web = web;
        this.mapper = mapper;
    }

    @Override
    protected Observable<WeatherResponseEntity> buildUseCaseObservable() {
        return getWeather().map(mapper::map).toObservable();
    }

    private Single<WeatherResponse> getWeather(){
        return getData().settlementId != -1?
                web.getWeather(getData().settlementId) :
                web.getWeather(getData().name);
    }
}
