package com.example.anatoliy.mvpweatherapp.model.data.db.room;

import android.util.Log;

import com.example.anatoliy.mvpweatherapp.model.data.db.DataBaseRepository;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public class DataBaseRepositoryImpl implements DataBaseRepository {
    private static final String TAG = DataBaseRepositoryImpl.class.getSimpleName();
    private final AppDatabase db;

    public DataBaseRepositoryImpl(AppDatabase db) {
        this.db = db;
    }

    @Override
    public Single<List<SettlementWeatherEntity>> getSavedSettlements() {
        return Single.fromCallable(() -> db.settlementDAO().getAll());
    }

    @Override
    public Completable saveSettlements(SettlementWeatherEntity... entities) {
        return Completable.fromAction(() -> db.settlementDAO().insertAll(entities));
    }

    @Override
    public Completable dropTable() {
        return Completable.fromAction(() -> db.settlementDAO().dropTable());
    }

    @Override
    public Completable deleteSettlement(SettlementWeatherEntity entity) {
        return Completable.fromAction(() -> db.settlementDAO().delete(entity));
    }

    @Override
    public Single<SettlementWeatherEntity> getSettlementById(long id) {
        return Single.fromCallable(() -> db.settlementDAO().findById(id));
    }
}
