package com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast;

import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;

public class WeatherResponseEntity {
    private final ExtendedWeather extendedWeather;
    private final SettlementWeatherEntity settlement;

    public WeatherResponseEntity(ExtendedWeather extendedWeather, SettlementWeatherEntity settlement) {
        this.extendedWeather = extendedWeather;
        this.settlement = settlement;
    }

    public ExtendedWeather getExtendedWeather() {
        return extendedWeather;
    }

    public SettlementWeatherEntity getSettlement() {
        return settlement;
    }
}
