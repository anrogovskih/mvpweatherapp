package com.example.anatoliy.mvpweatherapp.model.domain.interactor;

import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.ForecastResponse;
import com.example.anatoliy.mvpweatherapp.model.data.web.WebRepository;
import com.example.anatoliy.mvpweatherapp.model.domain.Mapper;
import com.example.anatoliy.mvpweatherapp.model.domain.UseCase;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.DailyForecast;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.ForecastResponseEntity;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;

public class GetForecastUseCase extends UseCase<ForecastResponseEntity, SettlementWeatherEntity> {

    private final WebRepository web;
    private final Mapper<ForecastResponse, ForecastResponseEntity> mapper;

    public GetForecastUseCase(Scheduler resultScheduler,
                              Scheduler sourceScheduler,
                              WebRepository web,
                              Mapper<ForecastResponse, ForecastResponseEntity> mapper) {
        super(resultScheduler, sourceScheduler);
        this.web = web;
        this.mapper = mapper;
    }

    @Override
    protected Observable<ForecastResponseEntity> buildUseCaseObservable() {
        return getForecast().map(mapper::map).toObservable();
    }

    private Single<ForecastResponse> getForecast(){
        return getData().settlementId != -1?
                web.getForecast(getData().settlementId) :
                web.getForecast(getData().name);
    }
}
