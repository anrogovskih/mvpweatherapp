package com.example.anatoliy.mvpweatherapp.model.domain.interactor;

import com.example.anatoliy.mvpweatherapp.model.data.db.DataBaseRepository;
import com.example.anatoliy.mvpweatherapp.model.domain.UseCase;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class DeleteAllSettlementsUseCase extends UseCase<Void, Void> {
    private final DataBaseRepository db;

    public DeleteAllSettlementsUseCase(Scheduler resultScheduler, Scheduler sourceScheduler, DataBaseRepository db) {
        super(resultScheduler, sourceScheduler);
        this.db = db;
    }

    @Override
    protected Observable<Void> buildUseCaseObservable() {
        return db.dropTable().toObservable();
    }
}
