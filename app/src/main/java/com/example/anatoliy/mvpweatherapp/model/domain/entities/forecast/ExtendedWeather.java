package com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast;

import com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.BaseViewHolder;
import com.example.anatoliy.mvpweatherapp.presentation.ui.enums.ViewHolderItemIds;

public class ExtendedWeather implements BaseViewHolder.Item {

    private long timeStamp;
    private double temperature;
    private double windSpeed;
    private double pressure;
    private long humidity;
    private String description;

    public double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public long getHumidity() {
        return humidity;
    }

    public void setHumidity(long humidity) {
        this.humidity = humidity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public int getViewType() {
        return ViewHolderItemIds.EXTENDED_WEATHER;
    }
}
