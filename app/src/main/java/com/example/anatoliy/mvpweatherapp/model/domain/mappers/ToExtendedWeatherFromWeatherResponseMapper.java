package com.example.anatoliy.mvpweatherapp.model.domain.mappers;

import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.Weather;
import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.WeatherResponse;
import com.example.anatoliy.mvpweatherapp.model.domain.Mapper;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.ExtendedWeather;

public class ToExtendedWeatherFromWeatherResponseMapper implements Mapper<WeatherResponse, ExtendedWeather> {
    @Override
    public ExtendedWeather map(WeatherResponse response) {
        ExtendedWeather weather = new ExtendedWeather();
        if (response.getWeather() != null && !response.getWeather().isEmpty()){
            Weather responseWeather = response.getWeather().get(0);
            weather.setDescription(responseWeather.getDescription());
        }
        if (response.getWind() != null){
            weather.setWindSpeed(response.getWind().getSpeed());
        }
        if (response.getMain() != null){
            weather.setPressure(response.getMain().getPressure());
            weather.setHumidity(response.getMain().getHumidity());
            weather.setTemperature(response.getMain().getTemp());
        }
        return weather;
    }
}
