package com.example.anatoliy.mvpweatherapp.model.domain.mappers;

import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.WeatherResponse;
import com.example.anatoliy.mvpweatherapp.model.domain.Mapper;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.ExtendedWeather;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.WeatherResponseEntity;

public class ToWeatherResponseEntityFromWeatherResponseMapper implements Mapper<WeatherResponse,WeatherResponseEntity> {

    private final Mapper<WeatherResponse, ExtendedWeather> extendedWeatherMapper;
    private final Mapper<WeatherResponse, SettlementWeatherEntity> settlementWeatherEntityMapper;

    public ToWeatherResponseEntityFromWeatherResponseMapper(Mapper<WeatherResponse, ExtendedWeather> extendedWeatherMapper,
                                                            Mapper<WeatherResponse, SettlementWeatherEntity> settlementWeatherEntityMapper) {
        this.extendedWeatherMapper = extendedWeatherMapper;
        this.settlementWeatherEntityMapper = settlementWeatherEntityMapper;
    }

    @Override
    public WeatherResponseEntity map(WeatherResponse response) {
        return new WeatherResponseEntity(
                extendedWeatherMapper.map(response),
                settlementWeatherEntityMapper.map(response)
        );
    }
}
