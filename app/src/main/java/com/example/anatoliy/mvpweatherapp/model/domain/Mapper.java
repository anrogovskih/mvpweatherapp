package com.example.anatoliy.mvpweatherapp.model.domain;

/**
 * Transforms one object to another
 * @param <From> - input object
 * @param <To> - output object
 */
public interface Mapper<From, To> {
    To map(From object);
}
