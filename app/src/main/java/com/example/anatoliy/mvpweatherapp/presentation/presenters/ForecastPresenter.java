package com.example.anatoliy.mvpweatherapp.presentation.presenters;

import android.text.TextUtils;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.anatoliy.mvpweatherapp.model.domain.UseCase;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.DailyForecast;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.ForecastResponseEntity;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.WeatherResponseEntity;
import com.example.anatoliy.mvpweatherapp.presentation.ui.enums.ContentState;
import com.example.anatoliy.mvpweatherapp.presentation.ui.mvp_views.ForecastView;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;

import java.util.List;

import io.reactivex.observers.DisposableObserver;

@InjectViewState
public class ForecastPresenter extends MvpPresenter<ForecastView> {
    private static final String TAG = "Test_LOG";
    private static final String TITLE_DEFAULT = "Add new settlement";

    private final UseCase<Void, SettlementWeatherEntity> saveSettlementUC;
    private final UseCase<ForecastResponseEntity, SettlementWeatherEntity> getDailyForecastUC;
    private final UseCase<SettlementWeatherEntity, Long> getSettlementByIdUC;
    private final UseCase<Void, SettlementWeatherEntity> deleteSettlementUC;
    private final UseCase<WeatherResponseEntity, SettlementWeatherEntity> getSettlementWeatherUC;

    private String query;
    private SettlementWeatherEntity settlement;

    private boolean isSet;

    public ForecastPresenter(UseCase<Void, SettlementWeatherEntity> saveSettlementUC,
                             UseCase<ForecastResponseEntity, SettlementWeatherEntity> getDailyForecastUC,
                             UseCase<SettlementWeatherEntity, Long> getSettlementByIdUC,
                             UseCase<Void, SettlementWeatherEntity> deleteSettlementUC,
                             UseCase<WeatherResponseEntity, SettlementWeatherEntity> getSettlementWeatherUC) {
        this.saveSettlementUC = saveSettlementUC;
        this.getDailyForecastUC = getDailyForecastUC;
        this.getSettlementByIdUC = getSettlementByIdUC;
        this.deleteSettlementUC = deleteSettlementUC;
        this.getSettlementWeatherUC = getSettlementWeatherUC;
    }

    /**
     * Sets id received from main activity. If it's a default value, sets view to editor's mode.
     */
    public void setSettlementId(long id){
        if (isSet) return;

        if (id != -1){
            getSettlementByIdUC.execute(new DisposableObserver<SettlementWeatherEntity>() {
                @Override
                public void onNext(SettlementWeatherEntity settlementWeatherEntity) {
                    settlement = settlementWeatherEntity;
                    query = settlementWeatherEntity.name;
                    getViewState().setTitle(settlementWeatherEntity.name);
                    getViewState().setEditMode(true);
                    showSettlementWeather();
                }

                @Override
                public void onError(Throwable e) {
                    getViewState().setTitle(TITLE_DEFAULT);
                    getViewState().setEditMode(false);
                    isSet = true;
                }

                @Override
                public void onComplete() {

                }
            }, id);
        }
        else {
            getViewState().setTitle(TITLE_DEFAULT);
            getViewState().setEditMode(false);
            getViewState().hideForecast();
            getViewState().hideWeather();
        }
    }

    /**
     * Invokes every time after text in input field is changed
     * @param query - text from input field
     */
    public void onQueryChanged(String query){
        this.query = query;
        settlement = null;
        getViewState().hideForecast();
        getViewState().hideWeather();
    }

    /**
     * Queries forecast for 5 days
     */
    public void getDailyForecast(){
        if (hasNoSettlement()) return;

        getViewState().setLoading(true);
        getDailyForecastUC.execute(new DisposableObserver<ForecastResponseEntity>() {
            @Override
            public void onNext(ForecastResponseEntity response) {
                settlement = response.getSettlementWeatherEntity();
                query = settlement.name;
                getViewState().showForecast(response.getDailyForecast());
                getViewState().setLoading(false);
            }

            @Override
            public void onError(Throwable e) {
                onErrorDefault(e);
            }

            @Override
            public void onComplete() {

            }
        }, getRequest());
    }

    public void saveSettlement(){
        if (hasNoSettlement()) return;

        saveSettlementUC.execute(new DisposableObserver<Void>() {
            @Override
            public void onNext(Void aVoid) {
            }

            @Override
            public void onError(Throwable e) {
                onErrorDefault(e);
            }

            @Override
            public void onComplete() {
                getViewState().setLoading(false);
                getViewState().finish();
            }
        }, getRequest());
    }

    public void deleteSettlement() {
        deleteSettlementUC.execute(new DisposableObserver<Void>() {
            @Override
            public void onNext(Void aVoid) {

            }

            @Override
            public void onError(Throwable e) {
                onErrorDefault(e);
            }

            @Override
            public void onComplete() {
                getViewState().finish();
            }
        }, settlement);
    }

    /**
     * Shows current weather in selected settlement
     */
    public void showSettlementWeather(){
        if (hasNoSettlement()) return;

        getViewState().setLoading(true);
        getSettlementWeatherUC.execute(new DisposableObserver<WeatherResponseEntity>() {
            @Override
            public void onNext(WeatherResponseEntity weatherResponseEntity) {
                getViewState().setLoading(false);
                settlement = weatherResponseEntity.getSettlement();
                getViewState().showWeather(weatherResponseEntity.getExtendedWeather());
                isSet = true;
            }

            @Override
            public void onError(Throwable e) {
                onErrorDefault(e);
            }

            @Override
            public void onComplete() {

            }
        }, getRequest());
    }

    /**
     * Hides extended weather forecast for clicked day
     */
    public void closeExtendedWeatherForItem(DailyForecast forecast){
        getViewState().closeExtendedWeatherForItem(forecast);
    }

    /**
     * Shows extended weather forecast for clicked day
     */
    public void openExtendedWeatherForItem(DailyForecast forecast){
        getViewState().openExtendedWeatherForItem(forecast);
    }

    /**
     * Universal error handler
     */
    private void onErrorDefault(Throwable e) {
        Log.e(TAG, "onError: ", e);
        getViewState().setLoading(false);
        if (e instanceof HttpException){
            switch (((HttpException)e).code()){
                case 404:
                    getViewState().showMessageUnknownSettlement();
                    return;
            }
        }
        getViewState().showMessageError();
    }

    /**
     * @return unified request with all the data we have for now
     */
    private SettlementWeatherEntity getRequest(){
        return settlement != null? settlement : new SettlementWeatherEntity(query);
    }

    private boolean hasNoSettlement(){
        if (TextUtils.isEmpty(query) && settlement == null){
            getViewState().showMessageEmptySettlement();
            return true;
        }
        else
            return false;
    }
}
