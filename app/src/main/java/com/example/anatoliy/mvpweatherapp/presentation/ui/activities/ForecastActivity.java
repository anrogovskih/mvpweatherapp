package com.example.anatoliy.mvpweatherapp.presentation.ui.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.example.anatoliy.mvpweatherapp.MyApplication;
import com.example.anatoliy.mvpweatherapp.R;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.DailyForecast;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.ExtendedWeather;
import com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.forecast.ForecastAdapter;
import com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.forecast.view_holders.ExtendedWeatherViewHolder;
import com.example.anatoliy.mvpweatherapp.presentation.ui.mvp_views.ForecastView;
import com.example.anatoliy.mvpweatherapp.presentation.presenters.ForecastPresenter;
import com.example.anatoliy.mvpweatherapp.utils.DialogBuilderUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;

import static com.example.anatoliy.mvpweatherapp.utils.Constants.LABEL_TAG;

public class ForecastActivity extends MvpAppCompatActivity implements ForecastView {
    private static final String TAG = ForecastActivity.class.getSimpleName();

    @InjectPresenter
    ForecastPresenter forecastPresenter;

    @ProvidePresenter
    public ForecastPresenter provideForecastPresenter(){
        return MyApplication.createForecastPresenter();
    }

    /** EditText field to enter the settlement's name */
    @BindView(R.id.edit_name) EditText mNameEditText;
    @BindView(R.id.id_progress_bar) ProgressBar mProgressBar;
    @BindView(R.id.id__recycler_view) RecyclerView mRecyclerView;
    @BindView(R.id.id__show_current_weather_button) View mShowForecastButton;
    @BindView(R.id.id__show_5_days_forecast_button) View mShowSeveralDaysForecastButton;
    @BindView(R.id.id__item_extended_weather) View extendedWeatherItemView;
    private ExtendedWeatherViewHolder weatherViewHolder;

    private Unbinder unbinder;

    boolean isInEditMode = false;

    private ForecastAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast);
        unbinder = ButterKnife.bind(this);
        weatherViewHolder = new ExtendedWeatherViewHolder(extendedWeatherItemView);

        Intent intent = getIntent();
        forecastPresenter.setSettlementId(intent.getLongExtra(LABEL_TAG, -1));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void setEditMode(boolean inEditMode) {
        isInEditMode = inEditMode;
        if (isInEditMode){
            mNameEditText.setVisibility(View.GONE);
            mShowForecastButton.setVisibility(View.GONE);
        }

        invalidateOptionsMenu();
    }

    @OnTextChanged(R.id.edit_name)
    void onSettlementNameChanged(CharSequence s){
        if (mNameEditText.isShown()) forecastPresenter.onQueryChanged(s.toString());
    }

    @OnClick(R.id.id__show_5_days_forecast_button)
    void showForecast(){
        forecastPresenter.getDailyForecast();
    }

    @OnClick(R.id.id__show_current_weather_button)
    void showWeather(){
        forecastPresenter.showSettlementWeather();
    }

    private void onForecastItemClicked(View view){
        if (view.getTag() instanceof DailyForecast){
            DailyForecast forecast = (DailyForecast) view.getTag();
            if (adapter.isOpen(forecast))
                forecastPresenter.closeExtendedWeatherForItem(forecast);
            else
                forecastPresenter.openExtendedWeatherForItem(forecast);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option
            case R.id.action_save:
                // Save settlement to database
                forecastPresenter.saveSettlement();
                return true;
            // Respond to a click on the "Delete" menu option
            case R.id.action_delete:
                showDeleteConfirmationDialog();
                return true;
            // Respond to a click on the "Up" arrow button in the app bar
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(ForecastActivity.this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDeleteConfirmationDialog() {
        DialogBuilderUtils.showDeleteConfirmationDialog(
                this,
                (DialogInterface dialog, int which)->{
                    forecastPresenter.deleteSettlement();
                    dialog.dismiss();
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_editor, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (isInEditMode) {
            MenuItem menuItem = menu.findItem(R.id.action_save);
            menuItem.setVisible(false);
        }
        // If this is a new settlement, hide the "Delete" menu item.
        else {
            MenuItem menuItem = menu.findItem(R.id.action_delete);
            menuItem.setVisible(false);
        }
        return true;
    }

    @Override
    public void showForecast(List<DailyForecast> forecast) {
        if (adapter == null)
            mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        weatherViewHolder.itemView.setVisibility(View.GONE);
        adapter = new ForecastAdapter();
        adapter.setData(forecast);
        adapter.setOnItemClickListener(this::onForecastItemClicked);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showWeather(ExtendedWeather weather) {
        mRecyclerView.setVisibility(View.GONE);
        weatherViewHolder.bind(weather);
        weatherViewHolder.itemView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showMessageEmptySettlement() {
        Toast.makeText(this, R.string.message_empty_query, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessageError() {
        Toast.makeText(this, R.string.network_error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMessageUnknownSettlement() {
        Toast.makeText(this, R.string.forecast_no_settlement_found, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void hideForecast() {
        mRecyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hideWeather() {
        weatherViewHolder.itemView.setVisibility(View.GONE);
    }

    @Override
    public void closeExtendedWeatherForItem(DailyForecast forecast) {
        if (adapter!= null) adapter.closeExtendedWeatherForItem(forecast);
    }

    @Override
    public void openExtendedWeatherForItem(DailyForecast forecast) {
        if (adapter!= null) adapter.openExtendedWeatherForItem(forecast);

    }
}
