package com.example.anatoliy.mvpweatherapp.model.data.db;

import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface DataBaseRepository {
    Single<List<SettlementWeatherEntity>> getSavedSettlements();
    Single<SettlementWeatherEntity> getSettlementById(long id);
    Completable saveSettlements(SettlementWeatherEntity... entities);
    Completable dropTable();
    Completable deleteSettlement(SettlementWeatherEntity entity);
}
