package com.example.anatoliy.mvpweatherapp.model.domain.entities.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.BaseViewHolder;

import java.util.Locale;

@Entity
public class SettlementWeatherEntity implements BaseViewHolder.Item{

    public SettlementWeatherEntity(String name) {
        this.name = name;
        settlementId = -1;
    }

    public SettlementWeatherEntity() {
    }

    /**
     * Name of the settlement.
     */
    @ColumnInfo(name = "name")
    public String name;

    /**
     * API id of the settlement.
     */
    @PrimaryKey
    public long settlementId;

    /**
     * Current temperature in the settlement in Kelvins.
     */
    @ColumnInfo(name = "temperature")
    public double currentTemperature;

    /**
     * A time of last update from network
     */
    @ColumnInfo(name = "update_time")
    public long updateTimeStamp;

    @Override
    public int getViewType() {
        return 0;
    }

    @Override @NonNull
    public String toString() {
        return String.format(Locale.getDefault(),
                "Name: %s, settlementId is %d, temperature is %.2f", name, settlementId, currentTemperature);
    }
}
