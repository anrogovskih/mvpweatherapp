package com.example.anatoliy.mvpweatherapp.model.domain.interactor;

import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.GroupsResponse;
import com.example.anatoliy.mvpweatherapp.model.data.db.DataBaseRepository;
import com.example.anatoliy.mvpweatherapp.model.data.web.WebRepository;
import com.example.anatoliy.mvpweatherapp.model.domain.Mapper;
import com.example.anatoliy.mvpweatherapp.model.domain.UseCase;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;

/**
 * Updates settlements in database, fetching data by their ids.
 * Returns list of updated settlements to display.
 */
public class UpdateSettlementsUseCase extends UseCase<List<SettlementWeatherEntity>, long[]> {

    private final DataBaseRepository db;
    private final WebRepository web;
    private final Mapper<GroupsResponse, List<SettlementWeatherEntity>> mapper;

    public UpdateSettlementsUseCase(Scheduler resultScheduler,
                                       Scheduler sourceScheduler,
                                       DataBaseRepository db,
                                       WebRepository web,
                                       Mapper<GroupsResponse, List<SettlementWeatherEntity>> mapper) {
        super(resultScheduler, sourceScheduler);
        this.db = db;
        this.web = web;
        this.mapper = mapper;
    }

    @Override
    protected Observable<List<SettlementWeatherEntity>> buildUseCaseObservable() {
        return web
                .getGroups(getIds())
                .map(mapper::map)
                .flatMapObservable(this::saveToDatabase);
    }

    private Observable<List<SettlementWeatherEntity>> saveToDatabase(List<SettlementWeatherEntity> entities){
        return db
                .saveSettlements(entities.toArray(new SettlementWeatherEntity[0]))
                .toSingle(() -> entities)
                .toObservable();
    }

    private String getIds(){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < getData().length; i++) {
            sb.append(getData()[i]);
            if (i < getData().length-1){
                sb.append(",");
            }
        }
        return sb.toString();
    }
}
