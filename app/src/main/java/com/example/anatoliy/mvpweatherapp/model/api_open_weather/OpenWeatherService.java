package com.example.anatoliy.mvpweatherapp.model.api_open_weather;

import com.example.anatoliy.mvpweatherapp.model.domain.http.HttpClient;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OpenWeatherService {
    protected static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";

    private static OpenWeatherService instance;
    public static OpenWeatherService getInstance() {
        if (instance == null) instance = new OpenWeatherService();
        return instance;
    }

//    protected ObjectMapper mapper;
    //    implementation 'com.squareup.retrofit2:converter-jackson:2.4.0'
    protected Retrofit retrofit;
    private Api api;

    private OpenWeatherService() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(HttpClient.getInstance())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        api = retrofit.create(Api.class);
    }

    public Api getApi() {
        return api;
    }
}
