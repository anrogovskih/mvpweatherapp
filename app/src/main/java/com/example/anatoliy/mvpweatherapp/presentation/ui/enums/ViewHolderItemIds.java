package com.example.anatoliy.mvpweatherapp.presentation.ui.enums;

import android.support.annotation.IntDef;

/**
 * Ids for using inside RecyclerView.Adapter
 */
@IntDef({})
public @interface ViewHolderItemIds {
    int DAILY_FORECAST = 1;
    int EXTENDED_WEATHER = 2;
}
