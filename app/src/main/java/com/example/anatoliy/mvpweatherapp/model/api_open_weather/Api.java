package com.example.anatoliy.mvpweatherapp.model.api_open_weather;

import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.ForecastResponse;
import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.GroupsResponse;
import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.WeatherResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {
    @GET("weather")
    Single<WeatherResponse> getWeather(@Query("appid") String appid, @Query("q")String query);

    @GET("weather")
    Single<WeatherResponse> getWeather(@Query("appid") String appid, @Query("id")long id);

    @GET("forecast")
    Single<ForecastResponse> getForecast(@Query("appid") String appid, @Query("q")String query);

    @GET("forecast")
    Single<ForecastResponse> getForecast(@Query("appid") String appid, @Query("id")long id);

    @GET("group")
    Single<GroupsResponse> getGroups(@Query("appid") String appid, @Query("id") String ids);
}
