package com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.forecast.view_holders;

import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.anatoliy.mvpweatherapp.R;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.DailyForecast;
import com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.BaseViewHolder;
import com.example.anatoliy.mvpweatherapp.utils.FormatUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DailyForecastViewHolder extends BaseViewHolder<DailyForecast> {
    @BindView(R.id.id_date)
    TextView dateTV;
    @BindView(R.id.id_day)
    TextView dayTV;
    @BindView(R.id.id_description)
    TextView descriptionTV;
    @BindView(R.id.id__temperature_max)
    TextView temperatureMaxTV;
    @BindView(R.id.id__temperature_min)
    TextView temperatureMinTV;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("d MMMM", Locale.getDefault());
    private SimpleDateFormat dateFormatDay = new SimpleDateFormat("EEEE", Locale.getDefault());

    private DailyForecastViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public DailyForecastViewHolder setOnClickListener(View.OnClickListener onClickListener){
        itemView.setOnClickListener(onClickListener);
        return this;
    }

    public static DailyForecastViewHolder newInstance (ViewGroup parent){
        return new DailyForecastViewHolder(createView(parent, R.layout.item_daily_forecast));
    }

    @Override
    public void bind(DailyForecast item) {
        super.bind(item);
        itemView.setTag(item);
        Date date = new Date(item.getDate());
        dateTV.setText(dateFormat.format(date));
        dayTV.setText(dateFormatDay.format(date));
        descriptionTV.setText(item.getDescription());
        temperatureMaxTV.setText(FormatUtils.getTemperatureCelsius(itemView.getContext(), item.getTemperatureMax()));
        temperatureMinTV.setText(FormatUtils.getTemperatureCelsius(itemView.getContext(), item.getTemperatureMin()));
    }
}
