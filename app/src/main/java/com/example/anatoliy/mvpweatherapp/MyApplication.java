package com.example.anatoliy.mvpweatherapp;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;

import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.Forecast;
import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.ForecastResponse;
import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.GroupsResponse;
import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.WeatherResponse;
import com.example.anatoliy.mvpweatherapp.model.data.db.DataBaseRepository;
import com.example.anatoliy.mvpweatherapp.model.data.db.room.AppDatabase;
import com.example.anatoliy.mvpweatherapp.model.data.db.room.DataBaseRepositoryImpl;
import com.example.anatoliy.mvpweatherapp.model.data.prefs.SharedRepository;
import com.example.anatoliy.mvpweatherapp.model.data.prefs.SharedRepositoryImpl;
import com.example.anatoliy.mvpweatherapp.model.data.web.WebRepository;
import com.example.anatoliy.mvpweatherapp.model.data.web.WebRepositoryImpl;
import com.example.anatoliy.mvpweatherapp.model.domain.Mapper;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.DailyForecast;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.ExtendedWeather;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.ForecastResponseEntity;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.WeatherResponseEntity;
import com.example.anatoliy.mvpweatherapp.model.domain.interactor.DeleteAllSettlementsUseCase;
import com.example.anatoliy.mvpweatherapp.model.domain.interactor.DeleteSettlementUseCase;
import com.example.anatoliy.mvpweatherapp.model.domain.interactor.GetForecastUseCase;
import com.example.anatoliy.mvpweatherapp.model.domain.interactor.GetSavedSettlementsUseCase;
import com.example.anatoliy.mvpweatherapp.model.domain.interactor.GetSettlementByIdUseCase;
import com.example.anatoliy.mvpweatherapp.model.domain.interactor.GetSettlementWeatherUseCase;
import com.example.anatoliy.mvpweatherapp.model.domain.interactor.SaveSettlementUseCase;
import com.example.anatoliy.mvpweatherapp.model.domain.interactor.UpdateSettlementsUseCase;
import com.example.anatoliy.mvpweatherapp.model.domain.mappers.ToDailyForecastFromForecastResponseMapper;
import com.example.anatoliy.mvpweatherapp.model.domain.mappers.ToExtendedWeatherFromForecastMapper;
import com.example.anatoliy.mvpweatherapp.model.domain.mappers.ToExtendedWeatherFromWeatherResponseMapper;
import com.example.anatoliy.mvpweatherapp.model.domain.mappers.ToForecastResponseEntityFromForecastResponseMapper;
import com.example.anatoliy.mvpweatherapp.model.domain.mappers.ToSettlementWeatherEntitiesFromGroupsResponseMapper;
import com.example.anatoliy.mvpweatherapp.model.domain.mappers.ToSettlementWeatherEntityFromForecastResponseMapper;
import com.example.anatoliy.mvpweatherapp.model.domain.mappers.ToSettlementWeatherEntityFromWeatherResponseMapper;
import com.example.anatoliy.mvpweatherapp.model.domain.mappers.ToWeatherResponseEntityFromWeatherResponseMapper;
import com.example.anatoliy.mvpweatherapp.presentation.presenters.MainPresenter;
import com.example.anatoliy.mvpweatherapp.presentation.presenters.ForecastPresenter;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MyApplication extends Application {

    private static final String SHARED_PREFERENCES_NAME = "SHARED_PREFERENCES_DEFAULT";
    private static final String DB_NAME = "settlements";

    private static DataBaseRepository db;
    private static SharedRepository prefs;
    private static WebRepository web;

    private static Mapper<WeatherResponse, SettlementWeatherEntity> toSettlementWeatherEntityFromWeatherResponseMapper
            = new ToSettlementWeatherEntityFromWeatherResponseMapper();
    private static Mapper<GroupsResponse, java.util.List<SettlementWeatherEntity>> toSettlementWeatherEntitiesFromGroupsResponseMapper
            = new ToSettlementWeatherEntitiesFromGroupsResponseMapper();
    private static Mapper<WeatherResponse, ExtendedWeather> toExtendedWeatherMapper
            = new ToExtendedWeatherFromWeatherResponseMapper();
    private static Mapper<Forecast, ExtendedWeather> toExtendedWeatherFromForecastMapper
            = new ToExtendedWeatherFromForecastMapper();
    private static Mapper<ForecastResponse, SettlementWeatherEntity> toSettlementWeatherEntityFromForecastResponseMapper
            = new ToSettlementWeatherEntityFromForecastResponseMapper();

    private static Mapper<WeatherResponse,WeatherResponseEntity> toWeatherResponseEntityMapper;
    private static Mapper<ForecastResponse, List<DailyForecast>> toDailyForecastFromForecastResponseMapper;
    private static Mapper<ForecastResponse, ForecastResponseEntity> toForecastResponseEntityMapper;


    @Override
    public void onCreate() {
        super.onCreate();
        db = new DataBaseRepositoryImpl(Room.databaseBuilder(getApplicationContext(), AppDatabase.class, DB_NAME).build());
        prefs = new SharedRepositoryImpl(getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE));
        web = new WebRepositoryImpl(getString(R.string.open_weather_map_api_key));
        AndroidThreeTen.init(this);

        toWeatherResponseEntityMapper = new ToWeatherResponseEntityFromWeatherResponseMapper(
                toExtendedWeatherMapper,
                toSettlementWeatherEntityFromWeatherResponseMapper);
        toDailyForecastFromForecastResponseMapper = new ToDailyForecastFromForecastResponseMapper(
                toExtendedWeatherFromForecastMapper);
        toForecastResponseEntityMapper = new ToForecastResponseEntityFromForecastResponseMapper(
                toDailyForecastFromForecastResponseMapper,
                toSettlementWeatherEntityFromForecastResponseMapper);
    }

    public static MainPresenter createMainPresenter(){
        return new MainPresenter(
                new GetSavedSettlementsUseCase(AndroidSchedulers.mainThread(), Schedulers.io(), db),
                new DeleteAllSettlementsUseCase(AndroidSchedulers.mainThread(), Schedulers.io(), db),
                new DeleteSettlementUseCase(AndroidSchedulers.mainThread(), Schedulers.io(), db),
                new UpdateSettlementsUseCase(AndroidSchedulers.mainThread(), Schedulers.io(), db, web, toSettlementWeatherEntitiesFromGroupsResponseMapper));
    }

    public static ForecastPresenter createForecastPresenter(){
        return new ForecastPresenter(
                new SaveSettlementUseCase(AndroidSchedulers.mainThread(), Schedulers.io(), db, web, toSettlementWeatherEntityFromWeatherResponseMapper),
                new GetForecastUseCase(AndroidSchedulers.mainThread(), Schedulers.io(), web, toForecastResponseEntityMapper),
                new GetSettlementByIdUseCase(AndroidSchedulers.mainThread(), Schedulers.io(), db),
                new DeleteSettlementUseCase(AndroidSchedulers.mainThread(), Schedulers.io(), db),
                new GetSettlementWeatherUseCase(AndroidSchedulers.mainThread(), Schedulers.io(), web, toWeatherResponseEntityMapper));
    }
}
