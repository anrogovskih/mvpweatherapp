package com.example.anatoliy.mvpweatherapp.model.data.prefs;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

public class SharedRepositoryImpl implements SharedRepository {

    private static final String UPDATE_KEY = "UPDATE_KEY";

    private final SharedPreferences sharedPref;

    public SharedRepositoryImpl(@NonNull SharedPreferences sharedPref) {
        this.sharedPref = sharedPref;
    }

    @Override
    public void setLastUpdateTimestamp(long lastUpdateTimestamp) {
        sharedPref.edit().putLong(UPDATE_KEY, lastUpdateTimestamp).apply();
    }

    @Override
    public long getLastUpdateTimestamp() {
        return sharedPref.getLong(UPDATE_KEY, 0);
    }
}
