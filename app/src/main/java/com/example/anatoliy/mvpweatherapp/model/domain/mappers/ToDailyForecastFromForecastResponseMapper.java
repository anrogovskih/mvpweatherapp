package com.example.anatoliy.mvpweatherapp.model.domain.mappers;

import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.Forecast;
import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.ForecastResponse;
import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.Weather;
import com.example.anatoliy.mvpweatherapp.model.domain.Mapper;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.DailyForecast;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.ExtendedWeather;

import org.threeten.bp.Instant;
import org.threeten.bp.LocalTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ToDailyForecastFromForecastResponseMapper implements Mapper<ForecastResponse, List<DailyForecast>> {
    private static final String TAG = ToDailyForecastFromForecastResponseMapper.class.getSimpleName();

    private final Mapper<Forecast, ExtendedWeather> toExtendedWeatherMapper;

    private Map<String, Integer> descriptionMap = new HashMap<>();

    public ToDailyForecastFromForecastResponseMapper(Mapper<Forecast, ExtendedWeather> toExtendedWeatherMapper) {
        this.toExtendedWeatherMapper = toExtendedWeatherMapper;
    }

    @Override
    public List<DailyForecast> map(ForecastResponse response) {
        ZonedDateTime startOfDay = ZonedDateTime.now().with(LocalTime.MIN);
        ZonedDateTime startOfNextDay = startOfDay.plusDays(1);
        Map<Long, DailyForecast> map = new HashMap<>();

        for (Forecast forecast : response.getForecast()) {
            ZonedDateTime forecastTime = Instant.ofEpochSecond(forecast.getDt()).atZone(ZoneId.systemDefault());
            //if we are still building same day forecast
            if (startOfDay.isBefore(forecastTime) && startOfNextDay.isAfter(forecastTime)) {
                mapForecast(forecast, startOfDay, map);
            }
            //if we should proceed to next day
            else if (startOfNextDay.isBefore(forecastTime)) {
                setDescription(map.get(startOfDay.toInstant().toEpochMilli()));
                startOfDay = startOfNextDay;
                startOfNextDay = startOfNextDay.plusDays(1);
                mapForecast(forecast, startOfDay, map);
            }
        }
        List<DailyForecast> result = new ArrayList<>(map.values());
        Collections.sort(result, (o1, o2) -> (int)(o1.getDate() - o2.getDate()));
        setDescription(result.get(result.size()-1));
        return result;
    }

    private void mapForecast(Forecast forecast, ZonedDateTime startOfDay, Map<Long, DailyForecast> map) {
        long timeStamp = startOfDay.toInstant().toEpochMilli();
        DailyForecast dailyForecast = map.get(timeStamp);
        if (dailyForecast == null)
            dailyForecast = createFrom(forecast, timeStamp);
        else
            update(dailyForecast, forecast);

        dailyForecast.add(toExtendedWeatherMapper.map(forecast));
        map.put(timeStamp, dailyForecast);
    }

    private void update(DailyForecast dailyForecast, Forecast forecast) {
        double tempMax = forecast.getMain().getTempMax();
        double tempMin = forecast.getMain().getTempMin();
        if (dailyForecast.getTemperatureMax() < tempMax)
            dailyForecast.setTemperatureMax(tempMax);
        if (dailyForecast.getTemperatureMin() > tempMin)
            dailyForecast.setTemperatureMin(tempMin);
        for (Weather weather:forecast.getWeather()) {
            addDescription(weather.getDescription());
        }
    }

    private DailyForecast createFrom(Forecast forecast, long timeStamp) {
        DailyForecast dailyForecast = new DailyForecast();
        dailyForecast.setDate(timeStamp);
        dailyForecast.setTemperatureMax(forecast.getMain().getTempMax());
        dailyForecast.setTemperatureMin(forecast.getMain().getTempMin());
        descriptionMap.clear();
        for (Weather weather:forecast.getWeather()) {
            addDescription(weather.getDescription());
        }
        return dailyForecast;
    }

    private void addDescription(String description){
        Integer count = descriptionMap.get(description);
        int newCount = count != null? (count + 1) : 1;
        descriptionMap.put(description, newCount);
    }

    /**
     * Sets description of weather of the day as description, that appeared more frequently
     */
    private void setDescription(DailyForecast dailyForecast){
        if (dailyForecast != null){
            int maxCount = 0;
            String descriptionOfTheDay = null;
            for(Map.Entry<String, Integer> pair : descriptionMap.entrySet()){
                if (pair.getValue() > maxCount){
                    maxCount = pair.getValue();
                    descriptionOfTheDay = pair.getKey();
                }
            }
            dailyForecast.setDescription(descriptionOfTheDay);
        }
    }
}
