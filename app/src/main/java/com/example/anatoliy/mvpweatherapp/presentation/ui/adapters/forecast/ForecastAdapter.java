package com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.forecast;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.DailyForecast;
import com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.BaseAdapter;
import com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.BaseViewHolder;
import com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.DefaultViewHolder;
import com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.forecast.view_holders.DailyForecastViewHolder;
import com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.forecast.view_holders.ExtendedDailyWeatherViewHolder;
import com.example.anatoliy.mvpweatherapp.presentation.ui.enums.ViewHolderItemIds;

import java.util.ArrayList;
import java.util.List;

public class ForecastAdapter extends BaseAdapter<BaseViewHolder> {

    private View.OnClickListener onItemClickListener;

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType){
            case ViewHolderItemIds.DAILY_FORECAST:
                return DailyForecastViewHolder.newInstance(parent).setOnClickListener(onItemClickListener);
            case ViewHolderItemIds.EXTENDED_WEATHER:
                return ExtendedDailyWeatherViewHolder.newInstance(parent);
        }
        return DefaultViewHolder.newInstance(parent);
    }

    public void setOnItemClickListener(View.OnClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setData(List<DailyForecast> forecasts) {
        items = new ArrayList<>();
        if (forecasts != null) {
            items.addAll(forecasts);
        }
    }

    public void closeExtendedWeatherForItem(DailyForecast forecast){
        closeAllItems();
    }

    public void openExtendedWeatherForItem(DailyForecast forecast){
        boolean isOpen = isOpen(forecast);
        closeAllItems();
        if (items != null && forecast.getHourlyForecast() != null && !isOpen){
            int index = items.indexOf(forecast);
            if (index != -1){
                items.addAll(index + 1, forecast.getHourlyForecast());
                notifyItemRangeInserted(index+1, forecast.getHourlyForecast().size());
            }
        }
    }

    public boolean isOpen(DailyForecast forecast){
        if (items != null && forecast != null){
            int index = items.indexOf(forecast);
            return index + 1 < items.size() && index != -1
                    && items.get(index+1).getViewType() == ViewHolderItemIds.EXTENDED_WEATHER;
        }
        return false;
    }

    public void closeAllItems(){
        if (items != null){
            for (int i = items.size()-1; i >= 0; i--) {
                if (items.get(i).getViewType() == ViewHolderItemIds.EXTENDED_WEATHER){
                    items.remove(i);
                    notifyItemRemoved(i);
                }
            }
        }
    }
}
