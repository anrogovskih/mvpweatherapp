package com.example.anatoliy.mvpweatherapp.presentation.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

public abstract class BaseViewHolder<I extends BaseViewHolder.Item> extends RecyclerView.ViewHolder {

    public interface Item{
        int getViewType();
    }

    public BaseViewHolder(@NonNull View itemView){
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @NonNull
    protected static View createView(ViewGroup parent, int layoutResId){
        return LayoutInflater.from(parent.getContext()).inflate(layoutResId, parent, false);
    }

    public void bind(I item){

    }
}
