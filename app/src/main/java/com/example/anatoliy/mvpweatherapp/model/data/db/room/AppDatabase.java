package com.example.anatoliy.mvpweatherapp.model.data.db.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;

@Database(entities = {SettlementWeatherEntity.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract SettlementDAO settlementDAO();
}
