package com.example.anatoliy.mvpweatherapp.presentation.ui.enums;

import android.support.annotation.StringDef;

import static com.example.anatoliy.mvpweatherapp.presentation.ui.enums.ContentState.EMPTY;
import static com.example.anatoliy.mvpweatherapp.presentation.ui.enums.ContentState.ERROR;
import static com.example.anatoliy.mvpweatherapp.presentation.ui.enums.ContentState.LOADING;
import static com.example.anatoliy.mvpweatherapp.presentation.ui.enums.ContentState.READY;

@StringDef({READY, LOADING, EMPTY, ERROR})
public @interface ContentState {

    /**
     * A content of page is shown
     */
    String READY = "READY";

    /**
     * A content of page is loading
     */
    String LOADING = "LOADING";

    /**
     * Nothing to show
     */
    String EMPTY = "EMPTY";

    /**
     * An error occurred while fetching data
     */
    String ERROR = "ERROR";
}
