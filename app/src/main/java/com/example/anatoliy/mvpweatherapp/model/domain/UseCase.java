package com.example.anatoliy.mvpweatherapp.model.domain;


import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public abstract class UseCase<Response, Data> {

    private final Scheduler resultScheduler;
    private final Scheduler sourceScheduler;

    private DisposableObserver<Response> observer;
    private Data data;

    public UseCase(Scheduler resultScheduler, Scheduler sourceScheduler) {
        this.resultScheduler = resultScheduler;
        this.sourceScheduler = sourceScheduler;
    }

    protected Data getData() {
        return data;
    }

    protected abstract Observable<Response> buildUseCaseObservable();

    public void execute(DisposableObserver<Response> observer, Data data){
        this.observer = observer;
        this.data = data;
        buildUseCaseObservable()
                .observeOn(resultScheduler)
                .subscribeOn(sourceScheduler)
                .subscribe(observer);
    }

    public void execute(DisposableObserver<Response> observer){
        this.observer = observer;
        buildUseCaseObservable()
                .observeOn(resultScheduler)
                .subscribeOn(sourceScheduler)
                .subscribe(observer);
    }

    public void execute(Data data){
        this.data = data;
        buildUseCaseObservable()
                .observeOn(resultScheduler)
                .subscribeOn(sourceScheduler)
                .subscribe();
    }

    public void execute(){
        buildUseCaseObservable()
                .observeOn(resultScheduler)
                .subscribeOn(sourceScheduler)
                .subscribe();
    }

    public void dispose(){
        if (observer != null) observer.dispose();
    }
}
