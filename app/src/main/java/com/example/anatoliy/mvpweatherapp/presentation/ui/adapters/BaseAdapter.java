package com.example.anatoliy.mvpweatherapp.presentation.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import java.util.List;

public abstract class BaseAdapter<V extends BaseViewHolder> extends RecyclerView.Adapter<V> {

    protected List<BaseViewHolder.Item> items;

    @Override
    public void onBindViewHolder(@NonNull V baseViewHolder, int i) {
        if (items != null) baseViewHolder.bind(items.get(i));
    }

    @Override
    public int getItemCount() {
        if (items != null) return items.size();
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (items != null) return items.get(position).getViewType();
        return super.getItemViewType(position);
    }

    /**
     * @param item - item to remove from adapter
     * @return true if item was successfully removed, false otherwise
     */
    public boolean removeItem(BaseViewHolder.Item item){
        if (items != null){
            int index = items.indexOf(item);
            if (index != -1){
                items.remove(index);
                notifyItemRemoved(index);
                return true;
            }
        }
        return false;
    }
}
