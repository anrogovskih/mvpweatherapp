package com.example.anatoliy.mvpweatherapp.model.domain.mappers;

import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.ForecastResponse;
import com.example.anatoliy.mvpweatherapp.model.domain.Mapper;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;

public class ToSettlementWeatherEntityFromForecastResponseMapper implements Mapper<ForecastResponse, SettlementWeatherEntity> {
    @Override
    public SettlementWeatherEntity map(ForecastResponse response) {
        SettlementWeatherEntity entity = new SettlementWeatherEntity();
        entity.settlementId = response.getCity().getId();
        entity.name = response.getCity().getName();
        entity.updateTimeStamp = System.currentTimeMillis();
        entity.currentTemperature = response.getForecast().get(0).getMain().getTemp();
        return entity;
    }
}
