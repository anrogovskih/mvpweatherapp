package com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast;

import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;

import java.util.List;

public class ForecastResponseEntity {
    private final List<DailyForecast> dailyForecast;
    private final SettlementWeatherEntity settlementWeatherEntity;

    public ForecastResponseEntity(List<DailyForecast> dailyForecast, SettlementWeatherEntity settlementWeatherEntity) {
        this.dailyForecast = dailyForecast;
        this.settlementWeatherEntity = settlementWeatherEntity;
    }

    public List<DailyForecast> getDailyForecast() {
        return dailyForecast;
    }

    public SettlementWeatherEntity getSettlementWeatherEntity() {
        return settlementWeatherEntity;
    }
}
