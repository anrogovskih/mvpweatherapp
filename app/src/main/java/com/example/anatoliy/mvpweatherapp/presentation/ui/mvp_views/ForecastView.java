package com.example.anatoliy.mvpweatherapp.presentation.ui.mvp_views;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.DailyForecast;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.ExtendedWeather;
import com.example.anatoliy.mvpweatherapp.presentation.ui.enums.ContentState;

import java.util.List;

public interface ForecastView extends MvpView {

    /**
     * Shows forecast for several days
     * @param forecast - each item is a forecast for 1 day
     */
    @StateStrategyType(AddToEndSingleStrategy.class)
    void showForecast(List<DailyForecast> forecast);

    /**
     * Shows current weather in settlement
     * @param weather - extended weather for current moment
     */
    @StateStrategyType(AddToEndSingleStrategy.class)
    void showWeather(ExtendedWeather weather);

    /**
     * Finishes scenario
     */
    @StateStrategyType(AddToEndSingleStrategy.class)
    void finish();

    @StateStrategyType(SkipStrategy.class)
    void showMessageEmptySettlement();

    @StateStrategyType(SkipStrategy.class)
    void showMessageError();

    @StateStrategyType(SkipStrategy.class)
    void showMessageUnknownSettlement();

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setTitle(CharSequence title);

    /**
     * @param inEditMode - true if we should show input field
     */
    @StateStrategyType(AddToEndSingleStrategy.class)
    void setEditMode(boolean inEditMode);

    /**
     * Shows and hides progress bar
     * @param isLoading - true if should show progress, false otherwise
     */
    @StateStrategyType(AddToEndSingleStrategy.class)
    void setLoading(boolean isLoading);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void hideForecast();

    @StateStrategyType(AddToEndSingleStrategy.class)
    void hideWeather();

    @StateStrategyType(AddToEndSingleStrategy.class)
    void closeExtendedWeatherForItem(DailyForecast forecast);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void openExtendedWeatherForItem(DailyForecast forecast);
}
