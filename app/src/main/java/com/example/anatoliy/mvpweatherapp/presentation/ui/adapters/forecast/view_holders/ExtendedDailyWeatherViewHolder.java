package com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.forecast.view_holders;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.anatoliy.mvpweatherapp.R;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.ExtendedWeather;
import com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.BaseViewHolder;
import com.example.anatoliy.mvpweatherapp.utils.FormatUtils;

import java.text.SimpleDateFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExtendedDailyWeatherViewHolder extends BaseViewHolder<ExtendedWeather> {

    @BindView(R.id.id__temperature) TextView temperatureTV;
    @BindView(R.id.id__humidity) TextView humidityTV;
    @BindView(R.id.id__wind) TextView windSpeedTV;
    @BindView(R.id.id__description) TextView descriptionTV;
    @BindView(R.id.id__date) TextView dateTV;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());

    private ExtendedDailyWeatherViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public static ExtendedDailyWeatherViewHolder newInstance(ViewGroup parent){
        return new ExtendedDailyWeatherViewHolder(createView(parent, R.layout.item_extended_daily_weather));
    }

    @Override
    public void bind(ExtendedWeather item) {
        super.bind(item);
        Context c = itemView.getContext();
        dateTV.setText(dateFormat.format(item.getTimeStamp()));
        temperatureTV.setText(FormatUtils.getTemperatureCelsius(c, item.getTemperature()));
        humidityTV.setText(c.getString(R.string.format_humidity, item.getHumidity()));
        windSpeedTV.setText(c.getString(R.string.format_wind_speed, item.getWindSpeed()));
        descriptionTV.setText(item.getDescription());
    }
}
