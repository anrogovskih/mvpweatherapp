
package com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GroupsResponse {

    @SerializedName("cnt")
    @Expose
    private long cnt;
    @SerializedName("list")
    @Expose
    private List<Forecast> list = null;

    public long getCnt() {
        return cnt;
    }

    public void setCnt(long cnt) {
        this.cnt = cnt;
    }

    public List<Forecast> getForecast() {
        return list;
    }

    public void setForecast(java.util.List<Forecast> forecast) {
        this.list = forecast;
    }

}
