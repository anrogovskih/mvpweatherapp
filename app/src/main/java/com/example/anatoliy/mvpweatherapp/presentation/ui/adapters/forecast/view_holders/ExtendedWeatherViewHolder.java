package com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.forecast.view_holders;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.example.anatoliy.mvpweatherapp.R;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.ExtendedWeather;
import com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.BaseViewHolder;
import com.example.anatoliy.mvpweatherapp.utils.FormatUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * View holder for using to display detailed information about current weather in selected settlement
 */
public class ExtendedWeatherViewHolder extends BaseViewHolder<ExtendedWeather> {

    @BindView(R.id.id__temperature) TextView temperatureTV;
    @BindView(R.id.id__humidity_value) TextView humidityTV;
    @BindView(R.id.id__wind_speed_value) TextView windSpeedTV;
    @BindView(R.id.id__description_value) TextView descriptionTV;
    @BindView(R.id.id__pressure_value) TextView pressureTV;

    public ExtendedWeatherViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void bind(ExtendedWeather item) {
        super.bind(item);
        Context c = itemView.getContext();
        temperatureTV.setText(FormatUtils.getTemperatureCelsius(c, item.getTemperature()));
        humidityTV.setText(c.getString(R.string.format_humidity, item.getHumidity()));
        windSpeedTV.setText(c.getString(R.string.format_wind_speed, item.getWindSpeed()));
        descriptionTV.setText(item.getDescription());
        pressureTV.setText(c.getString(R.string.format_pressure, item.getPressure()));
    }

}
