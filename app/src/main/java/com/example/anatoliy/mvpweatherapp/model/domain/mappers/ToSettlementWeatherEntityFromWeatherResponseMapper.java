package com.example.anatoliy.mvpweatherapp.model.domain.mappers;

import android.util.Log;

import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.WeatherResponse;
import com.example.anatoliy.mvpweatherapp.model.domain.Mapper;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;

public class ToSettlementWeatherEntityFromWeatherResponseMapper implements Mapper<WeatherResponse, SettlementWeatherEntity> {
    @Override
    public SettlementWeatherEntity map(WeatherResponse object) {
        SettlementWeatherEntity entity = new SettlementWeatherEntity();
        entity.currentTemperature = object.getMain().getTemp();
        entity.name = object.getName();
        entity.settlementId = object.getId();
        entity.updateTimeStamp = System.currentTimeMillis();
        return entity;
    }
}
