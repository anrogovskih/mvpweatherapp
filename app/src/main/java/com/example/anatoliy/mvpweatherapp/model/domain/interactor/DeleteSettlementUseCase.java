package com.example.anatoliy.mvpweatherapp.model.domain.interactor;

import com.example.anatoliy.mvpweatherapp.model.data.db.DataBaseRepository;
import com.example.anatoliy.mvpweatherapp.model.domain.UseCase;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class DeleteSettlementUseCase extends UseCase<Void, SettlementWeatherEntity> {

    private final DataBaseRepository db;

    public DeleteSettlementUseCase(Scheduler resultScheduler, Scheduler sourceScheduler, DataBaseRepository db) {
        super(resultScheduler, sourceScheduler);
        this.db = db;
    }

    @Override
    protected Observable<Void> buildUseCaseObservable() {
        return db.deleteSettlement(getData()).toObservable();
    }
}
