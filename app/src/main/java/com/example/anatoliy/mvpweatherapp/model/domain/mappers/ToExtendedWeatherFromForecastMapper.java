package com.example.anatoliy.mvpweatherapp.model.domain.mappers;

import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.Forecast;
import com.example.anatoliy.mvpweatherapp.model.domain.Mapper;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.forecast.ExtendedWeather;

public class ToExtendedWeatherFromForecastMapper implements Mapper<Forecast, ExtendedWeather> {
    @Override
    public ExtendedWeather map(Forecast object) {
        ExtendedWeather weather = new ExtendedWeather();
        weather.setTimeStamp(object.getDt()*1000);
        if (object.getMain() != null){
            weather.setTemperature(object.getMain().getTemp());
            weather.setHumidity(object.getMain().getHumidity());
            weather.setPressure(object.getMain().getPressure());
        }
        if (object.getWeather() != null && !object.getWeather().isEmpty()){
            weather.setDescription(object.getWeather().get(0).getDescription());
        }
        if (object.getWind() != null){
            weather.setWindSpeed(object.getWind().getSpeed());
        }
        return weather;
    }
}
