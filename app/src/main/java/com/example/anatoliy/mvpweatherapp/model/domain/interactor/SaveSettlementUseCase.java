package com.example.anatoliy.mvpweatherapp.model.domain.interactor;

import com.example.anatoliy.mvpweatherapp.model.api_open_weather.pojo.WeatherResponse;
import com.example.anatoliy.mvpweatherapp.model.data.db.DataBaseRepository;
import com.example.anatoliy.mvpweatherapp.model.data.web.WebRepository;
import com.example.anatoliy.mvpweatherapp.model.domain.Mapper;
import com.example.anatoliy.mvpweatherapp.model.domain.UseCase;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;

/**
 * Retrieves settlement weather data from server and saves it or just saves if we already have one
 */
public class SaveSettlementUseCase extends UseCase<Void, SettlementWeatherEntity> {
    private final DataBaseRepository db;
    private final WebRepository web;
    private final Mapper<WeatherResponse, SettlementWeatherEntity> mapper;

    public SaveSettlementUseCase(Scheduler resultScheduler,
                                 Scheduler sourceScheduler,
                                 DataBaseRepository db,
                                 WebRepository web,
                                 Mapper<WeatherResponse, SettlementWeatherEntity> mapper) {
        super(resultScheduler, sourceScheduler);
        this.db = db;
        this.web = web;
        this.mapper = mapper;
    }

    @Override
    protected Observable<Void> buildUseCaseObservable() {
        return getWeather().flatMapCompletable(db::saveSettlements).toObservable();
    }

    private Single<SettlementWeatherEntity> getWeather(){
        return getData().settlementId != -1? Single.just(getData()) : web.getWeather(getData().name).map(mapper::map);
    }
}
