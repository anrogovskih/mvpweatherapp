package com.example.anatoliy.mvpweatherapp.model.domain.http;

import com.example.anatoliy.mvpweatherapp.model.domain.http.interceptor.LoggingInterceptor;

import okhttp3.OkHttpClient;

public class HttpClient {
    private static OkHttpClient instance;

    public static OkHttpClient getInstance() {
        if (instance == null){
            instance = new OkHttpClient.Builder().addInterceptor(new LoggingInterceptor()).build();
        }
        return instance;
    }
}
