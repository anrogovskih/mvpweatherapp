package com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.main;

import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;
import com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.BaseAdapter;
import com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.main.view_holders.SettlementViewHolder;

import java.util.ArrayList;
import java.util.List;

public class MainAdapter extends BaseAdapter<SettlementViewHolder> {
    private static final String TAG = MainAdapter.class.getSimpleName();;
    private View.OnClickListener onClickListener;
    private View.OnLongClickListener onLongClickListener;

    @NonNull
    @Override
    public SettlementViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return SettlementViewHolder.newInstance(viewGroup, onClickListener, onLongClickListener);
    }

    public void setData(List<SettlementWeatherEntity> settlements) {
        items = new ArrayList<>();
        if (settlements != null) {
            items.addAll(settlements);
        }
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.onLongClickListener = onLongClickListener;
    }
}
