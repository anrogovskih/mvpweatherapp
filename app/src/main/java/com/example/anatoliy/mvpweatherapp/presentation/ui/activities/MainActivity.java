package com.example.anatoliy.mvpweatherapp.presentation.ui.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.example.anatoliy.mvpweatherapp.MyApplication;
import com.example.anatoliy.mvpweatherapp.R;
import com.example.anatoliy.mvpweatherapp.model.domain.entities.db.SettlementWeatherEntity;
import com.example.anatoliy.mvpweatherapp.presentation.ui.adapters.main.MainAdapter;
import com.example.anatoliy.mvpweatherapp.presentation.ui.enums.ContentState;
import com.example.anatoliy.mvpweatherapp.presentation.ui.mvp_views.MainView;
import com.example.anatoliy.mvpweatherapp.presentation.presenters.MainPresenter;
import com.example.anatoliy.mvpweatherapp.utils.DialogBuilderUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.example.anatoliy.mvpweatherapp.utils.Constants.LABEL_TAG;

public class MainActivity extends MvpAppCompatActivity implements MainView {
    private static final String TAG = MainActivity.class.getSimpleName();

    @InjectPresenter
    MainPresenter mainPresenter;

    @ProvidePresenter
    public MainPresenter providePresenter(){
        return MyApplication.createMainPresenter();
    }

    @BindView(R.id.id_progress_bar) ProgressBar mProgressBar;
    @BindView(R.id.id__warning) ViewGroup mWarningLayout;
    @BindView(R.id.fab) FloatingActionButton mFloatingActionButton;
    @BindView(R.id.id__warning_message_button) TextView mAgainButton;
    @BindView(R.id.id_scrollable_container) RecyclerView mScrollableContainer;
    @BindView(R.id.empty_view) View mEmptyView;

    private Unbinder unbinder;

    private @ContentState String state = ContentState.LOADING;

    private MainAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        mainPresenter.updateData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void setData(List<SettlementWeatherEntity> settlements) {
        if (adapter == null){
            adapter = new MainAdapter();
            adapter.setOnClickListener(this::onSettlementClicked);
            adapter.setOnLongClickListener(this::onSettlementLongClick);
        }
        adapter.setData(settlements);
        mScrollableContainer.setAdapter(adapter);
    }

    private void onSettlementClicked(View v){
        if (v.getTag() instanceof SettlementWeatherEntity){
            Intent intent = new Intent(MainActivity.this, ForecastActivity.class);
            intent.putExtra(LABEL_TAG, ((SettlementWeatherEntity)v.getTag()).settlementId);
            startActivity(intent);
        }
    }

    private boolean onSettlementLongClick(View v){
        if (v.getTag() instanceof SettlementWeatherEntity){
            SettlementWeatherEntity entity = (SettlementWeatherEntity) v.getTag();
            DialogBuilderUtils.showDeleteConfirmationDialog(
                    this,
                    (DialogInterface dialog, int which)->{
                        mainPresenter.deleteSettlement(entity);
                        adapter.removeItem(entity);
                        dialog.dismiss();
                        if (adapter.getItemCount() == 0){
                            mainPresenter.onLastItemDeleted();
                        }
                    });
        }
        return true;
    }

    @Override
    public void setState(@ContentState String state) {
        this.state = state;
        switch (state){
            case ContentState.LOADING:
                mProgressBar.setVisibility(View.VISIBLE);
                break;
            case ContentState.EMPTY:
                mEmptyView.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.INVISIBLE);
                break;
            case ContentState.READY:
                mProgressBar.setVisibility(View.INVISIBLE);
                mWarningLayout.setVisibility(View.GONE);
                mEmptyView.setVisibility(View.INVISIBLE);
                break;
            case ContentState.ERROR:
                mWarningLayout.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.INVISIBLE);
                break;
        }
    }

    @OnClick(R.id.fab)
    void addNewSettlement(){
        Intent intent = new Intent(MainActivity.this, ForecastActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.id__warning_message_button)
    void reloadData(){
        mainPresenter.updateData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_catalog, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Delete all entries" menu option
            case R.id.action_delete_all_entries:
                mainPresenter.clearSettlements();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setLoading(boolean isLoading){
        if (mProgressBar != null) mProgressBar.setVisibility(isLoading? View.VISIBLE : View.INVISIBLE);
    }
}
